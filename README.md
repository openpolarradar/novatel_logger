# novatel_loggenovatel_logger
Reads Novatel raw binary GNSS stream from serial port (autodetects novatel serial device) and writes this datastream to a file. Some tags are interpretted and printed to the stdout for monitoring purposes. Also, from serial port and makes (BESTPOSB, BESTVELB, TIMEB) available via TCP server in Sonntag's navigation guidance system format.

The filename includes the hostname and UTC time of initial creation.

The program also attempts to set the system date to match UTC time.

The program should generally be run as root because of access to serial ports.

Example of running program:


sudo chmod a+rwx /bin/date; sudo chown root:root /bin/date; sudo chmod u+s /bin/date;
sudo chmod a+rwx /arena/novatel_logger/novatel_logger; sudo chown root:root /arena/novatel_logger/novatel_logger; sudo chmod u+s /arena/novatel_logger/novatel_logger;
gnome-terminal --geometry=100x20 -- /arena/novatel_logger/novatel_logger $GNSS_LOGGER_TIME_VALID_THRESHOLD $GNSS_LOGGER_HEADING_OFFSET

GNSS_LOGGER_TIME_VALID_THRESHOLD: Number of seconds to wait before setting the system time to the received GNSS received GNSS time. This useful since the time stamps in the beginning can often be bad/incorrect due to the serial buffer containing bad data.

$GNSS_LOGGER_HEADING_OFFSET: Offset to add to the heading reported by the GNSS units. This affects the stdout and the information that is passed to Sonntag's software, but not what is recorded to disk.


The following standard Ubuntu packages must be installed (e.g. `sudo apt install PACKAGE`) to compile the source code (compiled on Ubuntu 20):

* libqwt-qt5-dev (and additional prompted packages)
* qt5-default (and additional prompted packages)

May be useful, but possibly not required:

* qtcreator (and additional prompted packages)
* libqt5serialport5-dev (and additional prompted packages)
* libqt5svg5-dev (and additional prompted packages)

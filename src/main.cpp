//
// To avoid having to type password in:
// sudo chmod a+rwx /arena/novatel_logger/novatel_logger; sudo chown root:root /arena/novatel_logger/novatel_logger; sudo chmod u+s /arena/novatel_logger/novatel_logger;
// sudo chmod a+rwx /bin/date; sudo chown root:root /bin/date; sudo chmod u+s /bin/date;
//

#include <QTextStream>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QCoreApplication>
#include <QProcessEnvironment>
#include <QSysInfo>

#include <QString>
#include <QDateTime>

#include <QTcpSocket>
#include <QTcpServer>
#include <QtNetwork>
#include <QHostAddress>

#include <fstream>

#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "gps.h"
#include "messages.h"

QTcpServer *nav_server;
QTcpSocket *nav_socket = NULL;
bool server_done = false;

QSerialPort *serial;
GPS gps;

int time_valid;
QString filename;

time_t last_heading_time = 0;
time_t last_ins_time = 0;
time_t last_good_time = 0;
time_t last_good_time_error = 0;

int TIME_VALID_THRESHOLD = 5;
double HEADING_OFFSET = 0.0;

BestPos pos;
BestVel vel;
BestTime gps_time;
Heading heading;
InsAtt ins;
double best_vel_heading = 0.0;

QString msg_bestpos = "\n", msg_bestvel = "\n", msg_time = "\n", msg_ins = "\n", msg_heading = "\n";
int num_ins = 0, num_heading = 0;

const qint64 bytes_available_threshold = 10000;
void loop()
{

    time_t cur_time;
    time(&cur_time);
    if (time_valid > TIME_VALID_THRESHOLD && ((double)cur_time) > ((double)last_good_time) + 2.1 && cur_time > last_good_time_error + 1)
    {
        QTextStream(stdout) << "\n"
                            << "ERROR!!!!\nBAD FOR " << cur_time - last_good_time << " SECONDS." << "\n\n";
        last_good_time_error = cur_time;
    }

    if (nav_socket != NULL && nav_socket->state() == QAbstractSocket::UnconnectedState)
    {
        QTextStream(stdout) << "  Disconnected\n";
        delete (nav_socket);
        nav_socket = NULL;
        server_done = false;
    }
    if (nav_server->hasPendingConnections())
    {
        if (!server_done)
        {
            QTextStream(stdout) << "Pending connection\n";
            server_done = true;

            nav_socket = nav_server->nextPendingConnection();

            if (nav_socket->state() == QAbstractSocket::ConnectedState)
            {
                QTextStream(stdout) << "  Connected\n";
            }
            else
            {
                QTextStream(stdout) << "  Failed to connect\n";
            }
        }
        else
        {
            QTextStream(stdout) << "Pending connection, but already busy.\n";
        }
    }

    qint64 bytes_available = serial->bytesAvailable();
    if (bytes_available)
    {
        uint8_t byte;
        if (bytes_available > bytes_available_threshold)
        {
            QTextStream(stdout) << "Serial buffer is filling up: " << bytes_available << " bytes in buffer." << Qt::endl;
        }

        // Read all bytes that are available in the buffer (up to a maximum of
        // 4096 bytes)
        uint8_t read_buffer[4096];
        qint64 read_size = bytes_available;
        if (read_size > 4096)
        {
            read_size = 4096;
        }
        qint64 num_read = serial->read((char *)read_buffer, read_size);
        if (time_valid > TIME_VALID_THRESHOLD)
        {
            std::ofstream fout;
            fout.open(filename.toLocal8Bit().constData(), std::ios::binary | std::ios::app);
            fout.write((char *)read_buffer, num_read);
            fout.close();
        }

        // Process the bytes in the buffer one byte at a time (using state
        // machine logic in "gps" GPS class)
        qint64 buffer_idx;
        for (buffer_idx = 0; buffer_idx < num_read; buffer_idx++)
        {
            byte = read_buffer[buffer_idx];

            // Since the start of the logger, enough time has passed with good
            // data, start writing to the output file
            // if (time_valid > TIME_VALID_THRESHOLD)
            // {
            //     std::ofstream fout;
            //     fout.open(filename.toLocal8Bit().constData(), std::ios::binary | std::ios::app);
            //     fout.write((char *)&byte, sizeof(byte));
            //     fout.close();
            // }

            if (gps.ingest(byte))
            {
                if (gps.get_message_id() != 0)
                {
                    QString test;
                    if (time_valid > TIME_VALID_THRESHOLD)
                    {
                    }
                }
                switch (gps.get_message_id())
                {
                case 42:
                {
                    pos = gps.parse_bestpos();

                    msg_bestpos = QString("BESTPOS: %1°N %2°E %3 m Age: %4 Stat: %9:%5-%6-%7-%8 Error %10-%11-%12\n")
                                      .arg((double)pos.latitude, 8, 'f', 4, QLatin1Char(' '))
                                      .arg((double)pos.longitude, 9, 'f', 4, QLatin1Char(' '))
                                      .arg((double)pos.height, 5, 'f', 1, QLatin1Char(' '))
                                      .arg((float)pos.sol_age, 1, 'g', 0, QLatin1Char(' '))
                                      .arg((uint8_t)pos.tracked_sats, 2, 10, QLatin1Char(' '))
                                      .arg((uint8_t)pos.solution_sats, 2, 10, QLatin1Char(' '))
                                      .arg((uint8_t)pos.sol_L1_sats, 2, 10, QLatin1Char(' '))
                                      .arg((uint8_t)pos.sol_multi_sats, 2, 10, QLatin1Char(' '))
                                      .arg((uint32_t)pos.sol_stat, 1, 10, QLatin1Char(' '))
                                      .arg((double)pos.lat_stdev, 4, 'f', 1, QLatin1Char(' '))
                                      .arg((double)pos.long_stdev, 4, 'f', 1, QLatin1Char(' '))
                                      .arg((double)pos.hgt_stdev, 4, 'f', 1, QLatin1Char(' '));

                    time(&last_good_time);
                    break;
                }
                case 99:
                {
                    vel = gps.parse_bestvel();
                    if (fabs(vel.hor_spd) < 1 && fabs(vel.vert_spd) < 1)
                    {
                        vel.trk_gnd = best_vel_heading;
                    }
                    else
                    {
                        best_vel_heading = vel.trk_gnd;
                    }

                    msg_bestvel = QString("BESTVEL: heading: %2   hor %1 m/s vert %3 m/s\n")
                                      .arg((double)vel.hor_spd, 3, 'f', 1, QLatin1Char(' '))
                                      .arg((double)vel.trk_gnd, 6, 'f', 1, QLatin1Char(' '))
                                      .arg((double)vel.vert_spd, 4, 'f', 1, QLatin1Char(' '));

                    QTextStream(stdout) << msg_time;
                    QTextStream(stdout) << msg_bestpos;
                    QTextStream(stdout) << msg_bestvel;
                    QTextStream(stdout) << msg_heading;
                    QTextStream(stdout) << msg_ins;

                    double nav_heading;
                    if (1)
                    {
                        // Just use bestvel heading
                        nav_heading = vel.trk_gnd;
                    }
                    else
                    {
                        if (ins.status == 3 && cur_time - last_ins_time <= 2)
                        {
                            nav_heading = ins.azimuth;
                        }
                        else if (heading.sol_stat == 0 && cur_time - last_heading_time <= 2)
                        {
                            // dualantenna heading
                            nav_heading = heading.heading;
                        }
                        else
                        {
                            // bestvel heading
                            nav_heading = vel.trk_gnd;
                        }
                    }
                    if (nav_socket != NULL && nav_socket->state() == QAbstractSocket::ConnectedState)
                    {
                        QString output =
                            QString::asprintf("20,%04d%02d%02d,%02d%02d%06.3f,%f,%f,%f,%f,%f,%f,%f\n",
                                              (int)gps_time.utc_year, (char)gps_time.utc_month, (char)gps_time.utc_day,
                                              (char)gps_time.utc_hour, (char)gps_time.utc_min, ((double)gps_time.utc_ms) / 1000.0,
                                              pos.latitude, pos.longitude, pos.height * 3.28083989501,                      // 3.28... to convert metres->feet
                                              nav_heading, vel.hor_spd * 1.94384449, vel.vert_spd * 196.850393701, ins.roll // 1.94... to convert m/s->knots, 196.85... to convert m/s->fpm
                            );
                        QTextStream(stdout) << "Nav: " << output;
                        nav_socket->write(output.toUtf8());
                    }
                    else
                    {
                        QTextStream(stdout) << "Nav: Not connected\n";
                    }
                    break;
                }
                case 101:
                {
                    gps_time = gps.parse_time();

                    msg_time = QString("TIME: Status: %1 %4_%3_%2 %5:%6:%7\n")
                                   .arg(gps_time.utc_stat, 2, 10, QLatin1Char(' '))
                                   .arg(gps_time.utc_day, 2, 10, QLatin1Char('0'))
                                   .arg(gps_time.utc_month, 2, 10, QLatin1Char('0'))
                                   .arg(gps_time.utc_year, 4, 10, QLatin1Char('0'))
                                   .arg(gps_time.utc_hour, 2, 10, QLatin1Char('0'))
                                   .arg(gps_time.utc_min, 2, 10, QLatin1Char('0'))
                                   .arg(((double)gps_time.utc_ms) / 1000.0, 4, 'f', 1, QLatin1Char('0'));

                    // QTextStream(stdout) << "TIME Status: " << gps_time.utc_stat << " " << "Year: " << gps_time.utc_year << " Month: " << gps_time.utc_month << " Day:" << gps_time.utc_day << " " << gps_time.utc_hour << ":" << gps_time.utc_min << ":" << ((double)gps_time.utc_ms)/1000 << "\n";
                    if (gps_time.utc_stat == 1 && time_valid < TIME_VALID_THRESHOLD)
                    {
                        time_valid++;
                        QTextStream(stdout) << msg_time;
                    }
                    if (gps_time.utc_stat == 1 && time_valid == TIME_VALID_THRESHOLD)
                    {
                        time_valid = TIME_VALID_THRESHOLD + 1;
                        QTextStream(stdout) << msg_time;

                        QString machine_hostname = QSysInfo::machineHostName();

                        QTextStream(stdout) << "**************** FOUND SUFFICIENT VALID TIMES TO ENSURE OLD BUFFER FLUSHED **************" << "\n";
                        filename = QString("/data/GPS_Novatel_raw_%7_%1%2%3_%4%5%6.gps")
                                       .arg((int)gps_time.utc_year, 4, 10, QLatin1Char('0'))
                                       .arg((char)gps_time.utc_month, 2, 10, QLatin1Char('0'))
                                       .arg((char)gps_time.utc_day, 2, 10, QLatin1Char('0'))
                                       .arg((char)gps_time.utc_hour, 2, 10, QLatin1Char('0'))
                                       .arg((char)gps_time.utc_min, 2, 10, QLatin1Char('0'))
                                       .arg((int)gps_time.utc_ms / 1000, 2, 10, QLatin1Char('0'))
                                       .arg(machine_hostname);

                        QTextStream(stdout) << "**************** RECORDING STARTING " << filename << " **************" << "\n";

                        QTextStream(stdout) << "**************** SETTING SYSTEM DATE AND TIME TO GPS TIME **************" << "\n";
                        QString set_date_cmd;
                        set_date_cmd = QString("date -u -s \"%1/%2/%3 %4:%5:%6\"")
                                           .arg((char)gps_time.utc_month, 2, 10, QLatin1Char('0'))
                                           .arg((char)gps_time.utc_day, 2, 10, QLatin1Char('0'))
                                           .arg((int)gps_time.utc_year, 4, 10, QLatin1Char('0'))
                                           .arg((char)gps_time.utc_hour, 2, 10, QLatin1Char('0'))
                                           .arg((char)gps_time.utc_min, 2, 10, QLatin1Char('0'))
                                           .arg((int)gps_time.utc_ms / 1000, 2, 10, QLatin1Char('0'));
                        QTextStream(stdout) << set_date_cmd << "\n";
                        int ret_val;
                        ret_val = system(set_date_cmd.toLocal8Bit().constData());
                        QTextStream(stdout) << "  Return value: " << ret_val << "\n";
                    }
                    break;
                }
                case 263:
                {
                    ins = gps.parse_insatt();
                    num_ins++;

                    QString ins_status_str;
                    switch (ins.status)
                    {
                    case 0:
                        ins_status_str = "INS_INACTIVE";
                        break;
                    case 1:
                        ins_status_str = "INS_ALIGNING";
                        break;
                    case 2:
                        ins_status_str = "INS_HIGH_VARIANCE";
                        break;
                    case 3:
                        ins_status_str = "INS_SOLUTION_GOOD";
                        break;
                    case 6:
                        ins_status_str = "INS_SOLUTION_FREE";
                        break;
                    case 7:
                        ins_status_str = "INS_ALIGNMENT_COMPLETE";
                        break;
                    case 8:
                        ins_status_str = "DETERMINING_ORIENTATION";
                        break;
                    case 9:
                        ins_status_str = "WAITING_INITIALPOS";
                        break;
                    case 10:
                        ins_status_str = "WAITING_AZIMUTH";
                        break;
                    case 11:
                        ins_status_str = "INITIALIZING_BIASES";
                        break;
                    case 12:
                        ins_status_str = "MOTION_DETECT";
                        break;
                    case 14:
                        ins_status_str = "WAITING_ALIGNMENTORIENTATION";
                        break;
                    default:
                        ins_status_str = "UNKNOWN_STATUS";
                        break;
                    }
                    msg_ins = QString("INSATT:  Heading: %1 Pitch: %2 Roll: %3 Status: %4-%5 %6\n")
                                  .arg((double)ins.azimuth, 6, 'f', 1, QLatin1Char(' '))
                                  .arg((double)ins.pitch, 4, 'f', 1, QLatin1Char(' '))
                                  .arg((double)ins.roll, 5, 'f', 1, QLatin1Char(' '))
                                  .arg((uint32_t)ins.status, 2, 10, QLatin1Char(' '))
                                  .arg(ins_status_str)
                                  .arg((uint32_t)num_ins, 7, 10, QLatin1Char(' '));
                    time(&last_ins_time);

                    break;
                }
                case 971:
                case 2042:
                {
                    heading = gps.parse_heading();
                    heading.heading = fmodf(heading.heading + HEADING_OFFSET, 360.0);
                    num_heading++;
                    QString heading_status_str;
                    switch (heading.sol_stat)
                    {
                    case 0:
                        heading_status_str = "SOL_COMP_GOOD";
                        break;
                    case 1:
                        heading_status_str = "INSUFFICIENT_OBS";
                        break;
                    case 2:
                        heading_status_str = "NO_CONVERGENCE";
                        break;
                    case 3:
                        heading_status_str = "SINGULARITY";
                        break;
                    case 4:
                        heading_status_str = "COV_TRACE";
                        break;
                    case 5:
                        heading_status_str = "TEST_DIST";
                        break;
                    case 6:
                        heading_status_str = "COLD_START";
                        break;
                    case 7:
                        heading_status_str = "V_H_LIMIT";
                        break;
                    case 8:
                        heading_status_str = "VARIANCE";
                        break;
                    case 9:
                        heading_status_str = "RESIDUALS";
                        break;
                    case 13:
                        heading_status_str = "INTEGRITY_WARNING";
                        break;
                    case 18:
                        heading_status_str = "PENDING";
                        break;
                    case 19:
                        heading_status_str = "INVALID_FIX";
                        break;
                    case 20:
                        heading_status_str = "UNAUTHORIZED";
                        break;
                    case 22:
                        heading_status_str = "INVALID_RATE";
                        break;
                    default:
                        heading_status_str = "UNKNOWN_STATUS";
                        break;
                    }
                    msg_heading = QString("HEADING: Heading: %1 Pitch: %2 Heading-stdev: %3 Pitch-stdev: %4 Status: %5-%6 HEADING_OFFSET %7 %8\n")
                                      .arg((float)heading.heading, 6, 'f', 1, QLatin1Char(' '))
                                      .arg((float)heading.pitch, 4, 'f', 1, QLatin1Char(' '))
                                      .arg((float)heading.heading_stdev, 5, 'f', 1, QLatin1Char(' '))
                                      .arg((float)heading.pitch_stdev, 5, 'f', 1, QLatin1Char(' '))
                                      .arg((uint32_t)heading.sol_stat, 2, 10, QLatin1Char(' '))
                                      .arg(heading_status_str)
                                      .arg((double)HEADING_OFFSET, 5, 'f', 1, QLatin1Char(' '))
                                      .arg((uint32_t)num_heading, 7, 10, QLatin1Char(' '));
                    time(&last_heading_time);
                    break;
                }
                default:
                    break;
                }
            }
            // QTextStream(stdout) << bit << Qt::endl;
        }
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication::setSetuidAllowed(true);

    QCoreApplication app(argc, argv);

    // Example for getting environment variables:
    // QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    // QString h1 = env.value("HOSTNAME", QString("default"));
    // QTextStream(stdout) << "h1 " << env.toStringList().value(0) << "\n";

    // Nav server: open listening port
    nav_server = new QTcpServer();
    QString host_name = "localhost";
    QHostAddress host;
    host.setAddress(host_name);
    int port = 4040;
    QTextStream(stdout) << "Trying to listen on " << host_name << ": " << port << "\n";
    nav_server->listen(host, port);
    QTextStream(stdout) << "Listening\n";

    QTextStream out(stdout);

    if (argc >= 3)
    {
        try
        {
            HEADING_OFFSET = QString(argv[2]).toDouble();
        }
        catch (void *exception)
        {
            out << "Second argument interpretation failure. Setting HEADING_OFFSET to default value of 0.0." << "\n";
            HEADING_OFFSET = 0.0;
        }
        if (HEADING_OFFSET < -360 || HEADING_OFFSET > 360)
        {
            out << "HEADING_OFFSET out of range. Setting HEADING_OFFSET to default value of 0.0." << "\n";
            HEADING_OFFSET = 0.0;
        }
    }
    else
    {
        HEADING_OFFSET = 0.0;
    }
    out << "HEADING_OFFSET: " << HEADING_OFFSET << "\n\n";
    if (argc >= 2)
    {
        try
        {
            TIME_VALID_THRESHOLD = QString(argv[1]).toInt();
        }
        catch (void *exception)
        {
            out << "First argument interpretation failure. Setting TIME_VALID_THRESHOLD to default value of 5." << "\n";
            TIME_VALID_THRESHOLD = 5;
        }
        if (TIME_VALID_THRESHOLD < 1 || TIME_VALID_THRESHOLD > 100)
        {
            out << "TIME_VALID_THRESHOLD out of range. Setting TIME_VALID_THRESHOLD to default value of 5." << "\n";
            TIME_VALID_THRESHOLD = 5;
        }
    }
    else
    {
        TIME_VALID_THRESHOLD = 5;
    }
    out << "TIME_VALID_THRESHOLD: " << TIME_VALID_THRESHOLD << "\n\n";

    time_valid = 0;
    int port_valid = 0;

    QString port_system_location = "/dev/ttyUSB0";

    out << "**************** SYSTEM SERIAL PORT INFO **************" << "\n";
    const auto serialPortInfos = QSerialPortInfo::availablePorts();

    out << "Total number of serial ports available: " << serialPortInfos.count() << Qt::endl
        << Qt::endl;

    out << "Listing serial ports:" << Qt::endl
        << Qt::endl;

    const QString blankString = "N/A";
    QString description;
    QString manufacturer;
    QString serialNumber;

    for (const QSerialPortInfo &serialPortInfo : serialPortInfos)
    {
        description = serialPortInfo.description();
        manufacturer = serialPortInfo.manufacturer();
        serialNumber = serialPortInfo.serialNumber();

        if ((description == "Novatel GPS Receiver" || description == "NovAtel GPS Receiver") && port_valid == 0)
        {
            port_valid = 1;
            out << "******************** USING THIS PORT ************************" << Qt::endl;
            port_system_location = serialPortInfo.systemLocation();
        }

        out << "Port: " << serialPortInfo.portName() << Qt::endl
            << "Location: " << serialPortInfo.systemLocation() << Qt::endl
            << "Description: " << (!description.isEmpty() ? description : blankString) << Qt::endl
            << "Manufacturer: " << (!manufacturer.isEmpty() ? manufacturer : blankString) << Qt::endl
            << "Serial number: " << (!serialNumber.isEmpty() ? serialNumber : blankString) << Qt::endl
            << "Vendor Identifier: " << (serialPortInfo.hasVendorIdentifier() ? QByteArray::number(serialPortInfo.vendorIdentifier(), 16) : blankString) << Qt::endl
            << "Product Identifier: " << (serialPortInfo.hasProductIdentifier() ? QByteArray::number(serialPortInfo.productIdentifier(), 16) : blankString) << Qt::endl
            << "Busy: " << (serialPortInfo.isBusy() ? "Yes" : "No") << Qt::endl
            << Qt::endl;
    }
    if (port_valid == 0)
    {
        out << "WARNING: DID NOT FIND VALID NOVATEL PORT!" << Qt::endl;
    }

    out << "**************** OPENING SERIAL PORT " << port_system_location << " **************" << Qt::endl;
    serial = new QSerialPort(port_system_location);
    // serial->setBaudRate(230400); // Not needed?
    serial->open(QIODevice::ReadWrite);
    serial->readAll();
    out << "BAUD: " << serial->baudRate() << Qt::endl;

    out << "**************** STARTING TO READ SERIAL PORT **************" << Qt::endl;
    QTimer *parseTimer = new QTimer();
    heading.sol_stat = -1;
    ins.roll = 0;
    ins.status = -1;
    QObject::connect(parseTimer, &QTimer::timeout, &loop);
    parseTimer->start();

    int ret_val = app.exec();

    out << "**************** EXITING **************" << "\n";

    return ret_val;
}
